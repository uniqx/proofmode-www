---
title: How to Install ProofMode Mobile App
description: Get the app for Android and iPhone
subtitle: Get the app for Android and iPhone
---

### iPhone and iPad (BETA)

ProofMode for iOS devices is currently available as a stable public beta through Apple's Testflight service.

Step 1: Install [Testflight from Apple](https://itunes.apple.com/us/app/testflight/id899247664?mt=8) to enable beta access

Step 2: Join the [ProofMode Testflight for iOS Devices](https://testflight.apple.com/join/X2cdrTrK)

### Android

ProofMode for Android is currently available in full, public release via Google Play, and also available as early access beta releases via Github.

Install from <a href="https://play.google.com/store/apps/details?id=org.witness.proofmode">Google Play</a>

Alternative: [Direct Download BETA install](https://guardianproject.info/releases/proofmode-latest.apk)

ProofMode is also available through the [Guardian Project F-Droid Repository](https://guardianproject.info/fdroid)




