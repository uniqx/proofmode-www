---
title: 'ProofMode for iOS'
subtitle: 'Free and open-source app for iPhone, iPad and MacOS'
date: 2021-12-10 00:00:00
##featured_image: '/images/screens/proofmode1.png'
excerpt: Learn more about what we are up to on the iOS platform.
---

ProofMode for iOS is currently [under development on Gitlab](https://gitlab.com/guardianproject/proofmode-ios/-/tree/main_npex).

There is a public [Testflight for iOS Devices](https://testflight.apple.com/join/X2cdrTrK) available.

<div class="gallery" data-columns="3">
	<img src="/images/screens/proofmode1.png">
	<img src="/images/screens/proofmode2.png">
	<img src="/images/screens/proofmode3.png">
</div>
