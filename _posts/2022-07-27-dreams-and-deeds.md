---
title: 'Of Dreams and Deeds'
date: 2022-07-27 12:00:00
featured_image: '/images/posts/colorscape.jpeg'
excerpt: "It's not a war of information, it's a war of dreams and images that ... animate information, make that information true, or make it a lie." 
---

__by Jack Fox Keen__

_"It's not a war of information, it's a war of dreams and images that ... animate information, make that information true, or make it a lie."_ – Rick Rowley, Big Noise Tactical,  as quoted in [_The Poetics of Resistance_](https://www.akpress.org/a-poetics-of-resistance-the-revolutionary-public-relations-of-the-zapatista-insurgency.html)

![](/images/posts/colorscape.jpeg)

Image by [Jasmine Co](https://www.jasminecoart.com/) (with permission and compensation)

Do dreams die when they come into existence and cease to be dreams? Or are they reincarnated, recycled into the seeds of new dreams? From these seeds we can imagine the genealogy of dreams, the family tree of aspirations and hopes and fantasies. We can trace the branches of ideas, their tangents as they spiral into new ideas, fractals of thoughts and sentiments. The spiraling growths have tangible outcomes. This is the cycle of the indicative mood to the subjunctive mood and vice versa, as delineated in our [previous blog post](https://proofmode.org/blog/of-moods-and-means).

What does it mean to be able to detect a mood? Mood provides context for proof. ProofMode has plans for adding an optional journal entry feature, where we provide activists an opportunity to make themselves known, to embed their identity into the evidence. We understand that some activists want to–and need to–stay anonymous, which is why we always have the option to choose your settings for which evidence to collect and which to omit, and ultimately, which to share. However, we are also aware that there are some activists who have the privilege of being public, and who willingly choose to act on that privilege.

By adding comments, hashtags, journal entries, whichever free-text form one chooses, one can add context to the image they’re sharing with the world. This adds texture, feelings, and tangibility to the visual. It tells a story. Now the image is part of someone’s journey, it is part of living history. A caption breathes life into the image, animating it. If someone provides a detailed caption or accompanying journal entry to an image, and those words are stored in an immutable format, that is an unchangeable eye-witness testimony that is time stamped onto the blockchain, permanently etched into the decentralized ledge.

We can imagine ProofMode as part of a [chain of custody](https://en.wikipedia.org/wiki/Chain_of_custody), the chronological documentation that records the sequence of analysis, including electronic evidence. When a ProofMode user generates their proof, they are adding this proof to this chronological documentation. For example, a user takes a picture of a police officer pointing a gun at a protestor. ProofMode generates an immutable timestamp for the existence of this file on their phone, as well as the metadata associated with this photo, including location and the angle with which the phone was oriented. We can prove that a person had _this_ evidence of _this_ photo, on _this_ smartphone, at _this_ time, with _this_ testimony.

Now the dreams are turning into deeds. The activist is in action. With the power of context, a user can contribute to what has been dubbed [Viral Justice](https://www.ruhabenjamin.com/viral-justice). Technology, social media, and social justice have become inextricably entwined, with ordinary people recording history and creating cataclysmic changes, as written in [Seen and Unseen](https://www.simonandschuster.com/books/Seen-and-Unseen/Marc-Lamont-Hill/9781982180393).

There are, of course, risks associated with adding more information. We are extremely cognizant of these risks, though we know we may have blindspots. We encourage any and all feedback regarding these blindspots, and we will explore threat modeling in a future blog post. In a similar vein to the [principles of the decentralized web](https://getdweb.net/principles/), we have principles of ethical data management as illustrated by our [Clean Insights](https://cleaninsights.org/) project. User privacy and informed consent is our top priority which is why we will only allow for opt-in features for any and all information, in addition to complete user-autonomy to share this information.

As the world continues to evolve, we hope our technology evolves with it, as well as our values. We want ProofMode to provide a way to embody those values, providing concrete evidence to abstract ideals. We believe there is power in using one’s words, along with their images, to tell a story and shape the future. With social media, history is reaching a turning point. It is no longer being solely written by the victors, but also the victims. Our goal is to amplify the voices of those witnessing and suffering abuses, and giving them a platform to share their lived experiences with the world. Citizen journalists are changing the way we think about news and media, and ProofMode is a necessary tool in this process.


